'use strict';

angular.module('myApp.item', ['ngAnimate'])

.controller('ItemCtrl', ['$scope', '$http', '$stateParams', 'cartStorage', 'phoneList', function($scope, $http, $stateParams, cartStorage, phoneList) {
	$http.get('/data/phones/' + $stateParams.phoneId + '.json')
    .success(function(data, status, headers, config) {
      $scope.phone = data;
      $scope.slides = $scope.phone.images;
      $scope.phone.reviews = $scope.phone.reviews || [];
      $scope.phone.index = phoneList.getIndex($scope.phone);
    })
    .error(function(data, status, headers, config) {
      console.log('phone failed to load [phones' + $stateParams.phoneId + '.json]')
    });

  $scope.addPhone = cartStorage.addPhone;
  $scope.toPhone = phoneList.setId;
  $scope.review = {};

  $scope.addReview = function(){
    $scope.review.createdOn = Date.now();
    $scope.phone.reviews.push($scope.review);
    $scope.review = {};
  };

  $scope.setCurrentImage = function (index) {
    $scope.currentImage = index;
  };

}]);