'use strict';

angular.module('myApp.menu')

.directive('filters', function() {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: 'modules/Menu/views/filters.html'
  };
});