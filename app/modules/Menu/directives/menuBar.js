'use strict';

angular.module('myApp.menu')

.directive('menuBar', function() {
  return {
    restrict: 'E',
    transclude: true,
    templateUrl: 'modules/Menu/views/menuBar.html'
  };
});