'use strict';

angular.module('myApp.menu', [])

.controller('MenuCtrl', ['$scope', '$location', 'filtration', 'cartStorage', function($scope, $location, filtration, cartStorage) {
  var unbindPhoneCreated = $scope.$on("priceChanged", handlePriceChanged),
      unbindStateChanged = $scope.$on('$stateChangeSuccess', handleStateChanged);

	function handlePriceChanged(event, price) {
    $scope.price += price;
  };

  function handleStateChanged() {
    $scope.path = $location.path();
  };

	this.searcher = filtration.changeSearchValue;

  $scope.orders = {
    'price': {
      name: 'Price',
      type: 'price'
    },
    'name': {
      name: 'Name',
      type: 'name'
    }
  };

  $scope.menuItems = {
    'home': {
  		name: 'Home',
  		path: ''
  	},
  	'about': {
  	  name: 'About',
  	  path: 'About'
  	},
  	'contactUs': {
  	  name: 'Contacts',
  	  path: 'Contacts'
  	}
  };

  $scope.path = $location.path();
  $scope.price = cartStorage.getPrice();

  $scope.setOrder = function(_order, reverse) {
    if (reverse) {
			_order = '-' + _order;
    };

    filtration.setOrder(_order);
  };
}]);