'use strict';

angular.module('myApp.cart', [])

.controller('CartCtrl', ['$scope', '$http', 'cartStorage', function($scope, $http, cartStorage) {
  $scope.phones = cartStorage.getPhones();
  $scope.price = cartStorage.getPrice();
	$scope.removePhone = cartStorage.removePhone;
}])

.controller("OrderCtrl", ['$scope', '$http', 'cartStorage', function($scope, $http, cartStorage) {
  $scope.$on("priceChanged", function (event, price) {
  	$scope.price += price;
  });

  $scope.price = cartStorage.getPrice();

  this.order = {};

  this.confirmOrder = function() {
    this.order = {};
  };
}]);