'use strict';

angular.module('myApp.home', [])

.controller('HomeCtrl', ['$scope', '$http', 'cartStorage', 'phoneList', function($scope, $http, cartStorage, phoneList) {
	var unbindOrderChanged = $scope.$on("orderChanged", handleOrderChanged),
		  unbindSearchValueChanged = $scope.$on("searchValueChanged", handleSearchValueChanged),
		  unbindPhonesLoaded = $scope.$on("phonesLoaded", handlePhonesLoaded);

	function handleOrderChanged(event, order) {
    $scope.order = order;
  };

  function handleSearchValueChanged( event, _searchValue ) {
		$scope.filterVal = _searchValue;
  };

	function handlePhonesLoaded (event, data) {
		$scope.phones = data;
	};

	$scope.phones = phoneList.getPhones();
	$scope.addPhone = cartStorage.addPhone;
	$scope.setIndex = phoneList.setIndex;
}]);