myApp.service('phoneList', function($rootScope, $location, $http) {
	var phones = [];

	$http.get('/data/phones/phones.json').success(function(data, status, headers, config) {
    phones = data;
    _.each(phones, function(phone, index) {
      phone.index = index;
    });
    $rootScope.$broadcast("phonesLoaded", phones);
  });

  this.getPhones = function () {
    return phones;
  };

	this.getIndex = function (_phone) {
		var phone = _.find(phones, function(phone){ return phone.id == _phone.id; });
		return phone.index;
	};

	this.setIndex = function (_phone, _index) {
		var index = _.findIndex(phones, function(phone){ return phone.id == _phone.id; });
    phones[index].index = _index;
	};

  this.setId = function(_index) {
	  if (_index == phones.length) {
	    var phone = _.find(phones, function(phone){ return phone.index == 0;});
	    $location.path('/data/phones/' + phone.id);
	  } else if (_index < 0) {
	      var phone = _.find(phones, function(phone){ return phone.index == phones.length - 1;});
	      $location.path('/data/phones/' + phone.id);
	    } else {
	        var phone = _.find(phones, function(phone){ return phone.index == _index; });
	        $location.path('/data/phones/' + phone.id);
	    }
  };
});