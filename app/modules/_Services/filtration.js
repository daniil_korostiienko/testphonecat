myApp.service('filtration', function($rootScope) {
  var searchValue = '',
      order = '';

  this.changeSearchValue = function(_searchValue) {
    searchValue = _searchValue;
    $rootScope.$broadcast("searchValueChanged", _searchValue);
  };

  this.setOrder = function(_order) {
      order = _order;
      $rootScope.$broadcast("orderChanged", order);
  	};
})