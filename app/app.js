'use strict';

var myApp = angular.module('myApp', [
  'ui.router',
  'ngAnimate',
  'myApp.home',
  'myApp.item',
  'myApp.menu',
  'myApp.cart'
])

.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

	$stateProvider
		.state('Home', {
      url: "/",
      templateUrl: 'modules/Home/views/home.html',
      controller: 'HomeCtrl'
    })

    .state('Item', {
      url: "/data/phones/:phoneId",
      templateUrl: "modules/Item/views/item.html",
      controller: 'ItemCtrl'
    })

    .state('Cart', {
      url: "/cart",
      templateUrl: "modules/Cart/views/cart.html",
      controller: 'CartCtrl'
    });
});
