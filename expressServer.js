var express = require('express'),
    app = express(),
    fs = require('fs');

app.use('/', express.static(__dirname + '/app'));
app.use('/data', express.static(__dirname + '/data'));


app.get('/', function (request, response) {
    response.sendFile('index.html', { root: __dirname });
});

app.listen(3000);

console.log("\nServer start on 127.0.0.1:3000\n");